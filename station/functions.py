# Useful functions

import random

def generate_code(code_size):
    """
        Generates a numeric code. That code's length is defined
        by this method's "code_size" argument.
    """
    c = 0
    code = ''

    while c < code_size:
        code += str(random.randint(0, 9))
        c += 1
    
    return code
