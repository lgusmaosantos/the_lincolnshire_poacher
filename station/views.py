# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .models import *
from .functions import *

from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse

import datetime

# Create your views here.

def index(request):
    """
        Requests the daily message and registers it to the database.
    """
    context = {}
    now = datetime.datetime.now()
    todays_message = Message.objects.filter(
        date_time__year=now.year,
        date_time__month=now.month,
        date_time__day=now.day,
    )
    
    if len(todays_message) > 0:
        ### DAILY MESSAGE ALREADY GENERATED ###
        todays_message = todays_message[0]
    else:
        ### GENERATING DAILY MESSAGE ###
        todays_message = Message(
            code=generate_code(settings.CODE_SIZE),
            agent_ip_address=request.META['REMOTE_ADDR']
        )
        todays_message.save()

    context['message'] = todays_message.code
    context['generated_at'] = todays_message.date_time

    # return render(request, 'station/index.html', context)
    return JsonResponse(context)
