# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

# Register your models here.

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('code', 'date_time', 'agent_ip_address')
    search_fields = ('date_time', 'code', 'agent_ip_address')
    readonly_fields = ('date_time', 'code', 'agent_ip_address')
