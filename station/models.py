# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Message(models.Model):
    """
        Message records on the database.
    """
    code = models.CharField(max_length=10)
    date_time = models.DateTimeField(auto_now_add=True)
    agent_ip_address = models.GenericIPAddressField()

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"
